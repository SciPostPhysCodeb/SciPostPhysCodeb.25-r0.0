# Codebase release 0.0 for DFT2kp

by João Victor V. Cassiano, Augusto de Lelis Araújo, Paulo E. Faria Junior, Gerson J. Ferreira

SciPost Phys. Codebases 25-r0.0 (2024) - published 2024-02-05

[DOI:10.21468/SciPostPhysCodeb.25-r0.0](https://doi.org/10.21468/SciPostPhysCodeb.25-r0.0)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.25-r0.0) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright João Victor V. Cassiano, Augusto de Lelis Araújo, Paulo E. Faria Junior, Gerson J. Ferreira.

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.25-r0.0](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.25-r0.0)
* Live (external) repository at [https://gitlab.com/dft2kp/dft2kp/-/tree/0.0.3](https://gitlab.com/dft2kp/dft2kp/-/tree/0.0.3)
